import { createStore,applyMiddleware } from "redux";
import { thunk } from "redux-thunk";
import Reducer from "./Thunk/reducer";

export const store = createStore(Reducer,applyMiddleware(thunk))
