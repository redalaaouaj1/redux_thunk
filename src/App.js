import { Provider } from 'react-redux';
import {store} from "./store"
import UserComponent from './Thunk/UserComponent';

function App(){

  return(
    <>
    <Provider store={store}>
      <UserComponent/>
    </Provider>
    </>
  )
}
export default App;
